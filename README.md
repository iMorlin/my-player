My player
=====================

I am building a web based player and use electronic and spotify api for this.

Electron
-----------------------------------

**For a quick start and familiarization with the electron**

This is a minimal Electron application based on the [Quickstart](https://electronjs.org/docs/tutorial/quick-start) in the Electron documentation.

The basic Electron application only needs these files:

package.json - Points to the main application file and lists its details and dependencies.
index.js - Launches the application and creates a browser window to display HTML. This is the main process of the application.
index.html - the web page to display. This is the process of rendering the application.

You can learn more about each of these components in the [Quickstart](https://electronjs.org/docs/tutorial/quick-start).

Using an electron
-----------------------------------

```bash
# Clone this repository
git clone https://gitlab.com/iMorlin/my-player.git
# Go to repository
cd "Your repository"
# Install dependencies
npm install
# Launch the application
npm start
```

Resources for Studying Electrons
-----------------------------------

- [electronicjs.org/docs](https://electronjs.org/docs) - all Electron documentation
- [Electron / electronic-quick-start](https://github.com/electron/electron-quick-start) - very simple starter application for Electron
- [electronic / simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for their further development
- [electronic / electronics-api-demos](https://github.com/electron/electron-api-demos) - Electron application that teaches you how to use Electron
- [hokein / electronic-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo applications for various Electron APIs

Spotify
-----------------------------------

To use the spotify api, you need to log in to [spotify for developer](https://developer.spotify.com/dashboard).
Next, you need to create your project.

To be able to use the api, you need to authorize and get a user token, an example is shown on this site https://developer.spotify.com/documentation/web-api/quick-start, or you can use ready-made authorization (TODO).

Resources for learning Spotify api
-----------------------------------

- [developer.spotify.com/documentation/web-api/reference](https://developer.spotify.com/documentation/web-api/reference) - all Spotify APIs
- [developer.spotify.com/documentation/web-api/quick-start](https://developer.spotify.com/documentation/web-api/quick-start) - an example of authorization

##Additional Information
-----------------------------------

**Since the project is web-based, you can use any text editor, for example:**

- [Visual Studio Code](https://code.visualstudio.com)
- [Notepad ++](https://notepad-plus-plus.org/downloads/)
